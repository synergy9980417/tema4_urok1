package dirfile;

import java.io.IOException;

public class MyCatalog extends AbstractFileAndCatalog {


   public MyCatalog(String path, String name) {

      super(path, name);
   }


   String type(){
      return "Каталог";
   }


   boolean isCreate() throws IOException {
      if (this.file.mkdir())
         return true;
      return false;
   }

}
