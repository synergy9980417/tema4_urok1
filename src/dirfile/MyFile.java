package dirfile;

import java.io.IOException;

public class MyFile extends AbstractFileAndCatalog {


    public MyFile(String path, String name) {
        super(path, name);
    }


    String type(){
        return "Файл";
    }

    boolean isCreate() throws IOException {
        if (this.file.createNewFile())
            return true;
        return false;
    }
}
