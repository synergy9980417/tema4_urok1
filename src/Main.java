//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.

import dirfile.AbstractFileAndCatalog;
import dirfile.MyCatalog;
import dirfile.MyFile;
import nasledovanie2.MyAdmin;
import nasledovanie2.MyModer;
import nasledovanie2.MyUser;
import nasledovanie3.BadMen;
import nasledovanie3.FrendMainHero;
import nasledovanie3.MainHero;

public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.

//        Урок 1. Наследование
//        1.
//        Реализуйте классы с наследованием: директория, файл

          MyFile myfile=new MyFile(null,"file.txt");
          MyCatalog mycatalog = new MyCatalog(null,"testcatalog");

        
//        2.
//        Реализуйте классы с наследованием: пользователь, администратор, модератор

        MyUser user=new MyUser("Василий");
        MyAdmin admin = new MyAdmin("Пётр");
        MyModer moder = new MyModer("Арсений");
//        3.
//        Реализуйте классы с наследованием: персонаж, главный герой, злодей, друг
//        главного героя
        MainHero mainhero=new MainHero("Август");
        BadMen bedmen=new BadMen("Крот");
        FrendMainHero frendmainhero=new FrendMainHero("Лука");

//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все те
//        хнические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов
//

    }
}